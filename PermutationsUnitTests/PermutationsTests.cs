﻿using Xunit;
using Permutations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Permutations.Tests
{
    public class PermutationsTests
    {
        [Theory()]
        [InlineData("aab", 2)]
        [InlineData("aaa", 0)]
        [InlineData("aabb", 8)]
        [InlineData("abcdefa", 3600)]
        [InlineData("abfdefa", 2640)]
        [InlineData("a", 1)]
        [InlineData("zzzzzzzz", 0)]
        [InlineData("aaab", 0)]
        [InlineData("aaabb", 12)]
        public void PermAloneTest(string str, double expected)
        {
            var sut = new Permutations();
            double permutations = sut.PermAlone(str);
            Assert.Equal(expected, permutations);
        }
    }
}