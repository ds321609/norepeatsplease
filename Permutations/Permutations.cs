﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Permutations
{
    public class Permutations
    {
        private List<string> _permutations = new List<string>();
        public double PermAlone(string str)
        {
            HeapAlgorithm(str.Length, str);
            FilterPermutations(str);
            return _permutations.Count;
        }

        private void FilterPermutations(string str)
        {
            string uniqueChars = FindUniqueCharacters(str);
            List<string> filteredPermutations = new List<string>();

            foreach (var permutation in _permutations)
            {
                bool match = Regex.IsMatch(permutation, @"(.)+\1");
                if (!match)
                {
                    filteredPermutations.Add(permutation);
                }
            }

            _permutations = filteredPermutations;

        }

        private string FindUniqueCharacters(string str)
        {
            string uniqueCharacters = string.Empty;

            foreach (char c in str)
            {
                int charIndex = uniqueCharacters.IndexOf(c);

                if (charIndex == -1)
                {
                    uniqueCharacters += c;
                }
            }

            return uniqueCharacters;
        }

        private void HeapAlgorithm(int initalSlots, string str)
        {
            List<int> stackState = new List<int>();
            string newStr = str;
            try
            {
                int i;
                for (i = 0; i < initalSlots; i++)
                {
                    stackState.Add(0);
                }
                _permutations.Add(newStr);
                i = 1;

                while (i < initalSlots)
                {
                    if (stackState[i] < i)
                    {
                        if (i % 2 == 0)
                        {
                            newStr = SwapCharacters(0, i, newStr);
                        }
                        else
                        {
                            newStr = SwapCharacters(stackState[i], i, newStr);
                        }

                        _permutations.Add(newStr);


                        stackState[i] += 1;
                        i = 1;
                    }
                    else
                    {
                        stackState[i] = 0;
                        i += 1;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static string SwapCharacters(int origin, int swap, string str)
        {
            string newStr = string.Empty;

            for (int i = 0; i < str.Length; i++)
            {
                if (i == origin)
                {
                    newStr += str[swap];
                }
                else if (i == swap)
                {
                    newStr += str[origin];
                }
                else
                {
                    newStr += str[i];
                }
            }

            return newStr;
        }
    }
}
